﻿using UnityEngine;
using System;
using System.IO;
using System.Data; 
using System.Collections;
using System.Collections.Generic;
using Mono.Data.SqliteClient; 
using System.Drawing.Printing;

public class DatabaseManager : MonoBehaviour
{
	private IDbConnection _connection;
	private IDbCommand _command;
	private IDataReader _reader;

	private string _currentPlayerCPF;

	public void Purge()
	{
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = string.Format(DBConstants.PURGE, "users");
		_command.ExecuteNonQuery();

		_command.CommandText = string.Format(DBConstants.PURGE, "exercises");
		_command.ExecuteNonQuery();

		_command.CommandText = string.Format(DBConstants.PURGE, "relatory");
		_command.ExecuteNonQuery();

		_command.Dispose();
        _command = null;
        _connection.Close();
        _connection = null;
	}

#region TEST METHODS
	private void Start()
	{
		TestCreateUser();
	}

	public void TestCreateUser()
	{
		//CreateNewUser("test-group");
		//GetCurrentUser();
		
		//InsertRelatory("teste", 1, "asdasd");
		//Purge();
	}

	public void PrintUsers()
	{
		Debug.Log("=======================================================================");
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = DBConstants.SELECT_ALL_USERS;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			string final = "ID: {0}, Group: {1}, Date: {2}, Points: {3}";
			Debug.Log(string.Format(final, _reader["id"], _reader["group"], _reader["date"], _reader["points"]));
		}

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
		Debug.Log("=======================================================================");
	}
#endregion

#region USER METHODS
	public void CreateNewUser(string group)
	{
		bool _hasPlayed = false;
		string sql;

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		sql = string.Format(DBConstants.INSERT_USER, group);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();
		
		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
	}

	public int GetCurrentUser()
	{
		int userID = 0;

		//Debug.Log("=======================================================================");
		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		_command.CommandText = DBConstants.SELECT_USER;
        IDataReader _reader = _command.ExecuteReader();
        while (_reader.Read())
		{
			string final = "ID: {0}, Group: {1}, Date: {2}, Points: {3}";
			//Debug.Log(string.Format(final, _reader["id"], _reader["group"], _reader["date"], _reader["points"]));
			userID = (int)_reader["id"];
		}

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
		//Debug.Log("=======================================================================");

		return userID;
	}

	public void UpdateUser()
	{
		int userID = GetCurrentUser();
		int points = FindObjectOfType<PointsManager>().points;
		int time = (int)FindObjectOfType<UITimer>().currentTime;

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
        _connection.Open();
		_command = _connection.CreateCommand();

		string sql = string.Format(DBConstants.UPDATE_USER, time, points, userID);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();	

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
	}

	public void InsertExercise(string type, int tries)
	{
		bool _hasPlayed = false;
		string sql;

		int userID = GetCurrentUser();

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		sql = string.Format(DBConstants.INSERT_EXERCISE, userID, type, tries);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
	}

	public void InsertRelatory(string exercise, int answered_correctly, string alternative)
	{
		bool _hasPlayed = false;
		string sql;

		int userID = GetCurrentUser();

		_connection = new SqliteConnection(DBConstants.DATABASE_NAME);
		_connection.Open();
		_command = _connection.CreateCommand();

		sql = string.Format(DBConstants.INSERT_RELATORY, userID, exercise, answered_correctly, alternative);
		_command.CommandText = sql;
		_command.ExecuteNonQuery();

		_command.Dispose();
		_command = null;
		_connection.Close();
		_connection = null;
	}
#endregion
}
