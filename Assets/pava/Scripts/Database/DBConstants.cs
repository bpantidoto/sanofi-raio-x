﻿using UnityEngine;

public class DBConstants
{
	public static string DATABASE_NAME = "URI=file:" + Application.streamingAssetsPath + "/DB/XRaySanofi.db";

	public const string PURGE = "DELETE FROM {0}";

	public const string SELECT_ALL_USERS = "SELECT * FROM users";

	public const string INSERT_USER = "INSERT INTO users ('group') VALUES ('{0}')";
	public const string SELECT_USER = "SELECT * FROM users ORDER BY id DESC LIMIT 1";
	public const string UPDATE_USER = "UPDATE users SET time={0}, points={1} WHERE id={2}";


	public const string INSERT_EXERCISE = "INSERT INTO exercises ('user','type','tries') VALUES ({0},'{1}',{2})";

	public const string INSERT_RELATORY = "INSERT INTO relatory ('user','exercise','answered_correctly','alternative') VALUES ({0},'{1}',{2},'{3}')";

	public static string getStreamingAssetsPath()
	{
		return Application.streamingAssetsPath;
	}
}
