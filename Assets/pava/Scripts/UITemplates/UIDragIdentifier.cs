﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDragIdentifier : UIDragHandler 
{
	public int ID = 0;
    [HideInInspector]
    public bool shouldReset = true;

    public string alternativeIdentifier;
    public string exerciseIdentifier;

    private void Awake()
    {
        base.Awake();
        onBeginDrag.AddListener(delegate{BeginDrag();});
        onEndDrag.AddListener(delegate{EndDrag();});
    }

    private void BeginDrag()
    {
        Debug.Log("BeginDrag");
    }

    private void EndDrag()
    {
        Debug.Log("EndDrag");
        if(shouldReset)
        {
            ResetPosition();
        }
    }
}
