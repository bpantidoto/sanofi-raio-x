﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDropHandler : MonoBehaviour, IDropHandler
{
	public CustomEvent onDrop;

#region IDropHandler implementation
	public void OnDrop (PointerEventData eventData)
	{
		if(onDrop != null)
		{
			onDrop.Invoke();
		}
	}
#endregion
}
