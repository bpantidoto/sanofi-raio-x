﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIQuantityFeedback : MonoBehaviour 
{
	public Text quantityText;
	public Text correctText;

	public UIAnimateScale positiveImage;
	public UIAnimateScale negativeImage;

	private CanvasGroup _canvasGroup;
	private Image _image;

	public void SetFeedback(bool isPositive, int quantity)
	{	
		quantityText.text = "";
		_canvasGroup = GetComponent<CanvasGroup>();
		_image = GetComponent<Image>();

		_image.enabled = !isPositive; 
		correctText.gameObject.SetActive(!isPositive);
		quantityText.gameObject.SetActive(!isPositive);

		

		positiveImage.SetupScale();
		negativeImage.SetupScale();

		_canvasGroup.alpha = 0;
		_canvasGroup.DOFade(1f, 0.3f).OnComplete(delegate{
			if(isPositive)
			{
				positiveImage.AnimateIn();
			}else{
				negativeImage.AnimateIn();
				quantityText.text = quantity.ToString();
			}
		});
	}

	public void Reset()
	{
		_image = GetComponent<Image>();
		_image.enabled = false; 
		correctText.gameObject.SetActive(false);
		quantityText.gameObject.SetActive(false);
	}
}
