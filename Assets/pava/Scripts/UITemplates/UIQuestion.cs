﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIQuestion : MonoBehaviour 
{
	public Text textQuestionTitle;
	public Text[] textAnswers;

	public int correctAnswer = 0;

	private Button[] _buttons;

	private UIFeedback _feedback;

	private CanvasGroup _canvasGroup;

	private void Awake()
	{
		_buttons = gameObject.GetComponentsInChildren<Button>();
		_feedback = gameObject.GetComponentInChildren<UIFeedback>();
		_canvasGroup = gameObject.GetComponent<CanvasGroup>();
	}

	public void Setup()
	{
		//TODO: pull from xml or something like
		textQuestionTitle.text = "asdasdasdasdasd";

		int i = 0;
		foreach (Text current in textAnswers)
		{
			current.text = "answer_"+i.ToString();
			i++;
		}
	}

	public void SelectAnswer(int index)
	{
		//Debug.Log("ESCOLHEU: "+index.ToString());
		if(index == correctAnswer)
		{
			PositiveFeedback();
		}else{
			NegativeFeedback();
		}
		ToggleButtonsInteractable(false);
	}	

	private void PositiveFeedback()
	{	
		ApplyFeedack(true);
	}

	private void NegativeFeedback()
	{
		ApplyFeedack(false);
	}

	private void ApplyFeedack(bool isPositive)
	{
		_feedback.gameObject.SetActive(true);
		
		_feedback.OnAnimateInComplete.AddListener(delegate{
			Debug.Log("In Complete");
		});

		_feedback.OnAnimateOutComplete.AddListener(delegate{
			Debug.Log("Out Complete");
			_feedback.gameObject.SetActive(false);

			_feedback.OnAnimateInComplete.RemoveAllListeners();
			_feedback.OnAnimateOutComplete.RemoveAllListeners();

			_canvasGroup.DOFade(0f, 1f);
		});


		//TODO:
		//_feedback.SetFeedback(isPositive);
		_feedback.AnimateIn();
	}

	public void ToggleButtonsInteractable(bool isInteractable)
	{
		foreach(Button current in _buttons)
		{
			current.interactable = isInteractable;
		}
	}
}
