﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFinalScreen : MonoBehaviour 
{
	public Text pointsText;
	private string _defaultText;

	public ExercicePoints exercisePoints;

	private CanvasGroup _canvasGroup;

	private bool _arePointsSet = false;

	public string screenType = "";

	private void Awake()
	{
		_defaultText = pointsText.text;
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	public void SetPoints(int quantity)
	{
		pointsText.text = string.Format(_defaultText, quantity);
		SavePoints(quantity);
		_arePointsSet = true;
	}

	private void Update()
	{
		if(_canvasGroup.alpha > 0 && exercisePoints != null && !_arePointsSet)
		{
			SetPoints(exercisePoints.CalculatePoints());
		}
	}

	public void SavePoints(int quantity)
	{
		DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();
		databaseManager.InsertExercise (screenType, exercisePoints.GetTries ()); 
	}
}
