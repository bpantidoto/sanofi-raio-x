﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour 
{
	private Text _textfield;

	public string defaultLabel = "Tempo: {0}";


	public float initialTimeInMinutes = 12f;
	public bool isDeacreasing = true;

	private float _currentTime = 0f;
	public float currentTime 
	{
		get {
			return _currentTime;
		}
	}
	private bool _isTimeRunning = true;

	public CustomEvent onTimerComplete;

	private void Awake() 
	{
		_textfield = GetComponent<Text>();

		ResetTime();
		StopTimer();
		FormatTime();
	}
	
	private void Update()
	{
		if(_isTimeRunning)
		{
			if(isDeacreasing)
			{
				_currentTime -= Time.deltaTime;
				if(_currentTime <= 0)
				{
					StopTimer();
					_currentTime = 0f;
					onTimerComplete.Invoke();
				}
			}else{
				_currentTime += Time.deltaTime;
			}
		}

		FormatTime();
	}

	private void FormatTime()
	{
		int minutes = (int)(_currentTime / 60);
		int seconds = (int)(_currentTime % 60);
		string formattedTime = string.Format("{0:00}:{1:00}", minutes, seconds);
		_textfield.text = string.Format(defaultLabel, formattedTime);
	}

	public void StartTimer()
	{
		_isTimeRunning = true;
	}

	public void StopTimer()
	{
		_isTimeRunning = false;
	}

	public void ResetTime()
	{
		_currentTime = initialTimeInMinutes * 60f;
	}
}
