﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupQuestion : UIPopup 
{
	public Text questionTitleTextfield;
	public Text[] alternativeTextfields;

	private Question _question;

	private bool _isCorrect = false;
	public bool isCorrect {
		get {
			return _isCorrect;
		}
	}

	private UIFeedback _feedback;	
	private UITouchlocker _touchlocker;

	public int chosenAnswer = 0;

	private void Start()
	{
		_feedback = gameObject.GetComponentInChildren<UIFeedback>();
		_touchlocker = GameObject.FindGameObjectWithTag("Touchlocker").GetComponent<UITouchlocker>();
	}

	public void AssignQuestion(Question question)
	{
		_question = question;

		for(int i = 0; i < alternativeTextfields.Length; i++)
		{
			alternativeTextfields[i].text = question.answers[i];
		}

		questionTitleTextfield.text = question.title;
	}

	public void ChooseAnswer(int answerIndex)
	{
		chosenAnswer = answerIndex;
		
		if(answerIndex == _question.correctAnswer)
		{
			_isCorrect = true;
			//PointsManager.Instance.AddQuizPoints(1);
		}else{
			//PointsManager.Instance.SubtractQuizPoints(1);
			_isCorrect = false;
		}

		_touchlocker.DisableTouch();
		ApplyFeedack(answerIndex, _question.correctAnswer);
	}

	private void ApplyFeedack(int chosen, int correct)
	{
		_feedback.gameObject.SetActive(true);
		
		_feedback.OnAnimateInComplete.AddListener(delegate{
			Debug.Log("In Complete");
		});

		_feedback.OnAnimateOutComplete.AddListener(delegate{
			Debug.Log("Out Complete");
			_feedback.gameObject.SetActive(false);

			_feedback.OnAnimateInComplete.RemoveAllListeners();
			_feedback.OnAnimateOutComplete.RemoveAllListeners();

			_touchlocker.EnableTouch();

			ClosePopup();
		});

		_feedback.SetFeedback(chosen, correct);
		_feedback.AnimateIn();
	}
}