﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIPlusPoints : MonoBehaviour 
{
	public Color plusColor;
	public Color minusColor;

	private Text _textfield;
	private RectTransform _rectTransform;

	private void Awake()
	{
		_textfield = GetComponent<Text>();
		SetTextAndAnimate(100);
	}

	public void SetTextAndAnimate(int amount, bool increase=true)
	{
		if(increase)
		{
			_textfield.text = "+"+amount.ToString();
			_textfield.color = plusColor;
		}else{
			_textfield.text = "-"+amount.ToString();
			_textfield.color = minusColor;
		}
		Animate();
	}

	private void Animate()
	{
		//THIS IS NOT WORKING MAYBE PARENTING TO A GAME OBJECT WILL SOLVE IT
		_rectTransform.DOScale(new Vector3(1,1,1), 0.5f);
	}
}
