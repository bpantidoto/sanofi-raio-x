﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIFeedback : MonoBehaviour
{
	public GameObject[] positives;
	public GameObject[] negatives;

	public CustomEvent OnAnimateInComplete;
	public CustomEvent OnAnimateOutComplete;

	private bool _isWaitingInteraction = false;
	public bool isInteractable = true;

	private CanvasGroup _canvasGroup;

	private void Awake() 
	{
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	private void Start() 
	{
		_canvasGroup.alpha = 0;
		gameObject.SetActive(false);

		Reset();
	}

	private void Reset()
	{
		foreach (GameObject current in positives)
		{
			current.SetActive(false);
		}

		foreach (GameObject current in negatives)
		{
			current.SetActive(false);
		}
	}

	public void SetFeedback(int chosen, int correct)
	{
		if(chosen == correct)
		{
			positives[chosen].SetActive(true);
		}
		else{
			negatives[chosen].SetActive(true);
			positives[correct].SetActive(true);
		}
	}

	public void AnimateIn()
	{
		_canvasGroup.DOFade(1f, 0.5f).OnComplete(delegate{
			_isWaitingInteraction = true;
			OnAnimateInComplete.Invoke();
			StartCoroutine(Finish());
		});
	}

	private IEnumerator Finish()
	{
		yield return new WaitForSeconds(1f);
		AnimateOut();
	}

	public void AnimateOut()
	{
		_canvasGroup.DOFade(0f, 0.5f).OnComplete(delegate{
			OnAnimateOutComplete.Invoke();
			Reset();
		});
	}

	/* 
	public void OnPointerClick(PointerEventData pointerEventData)
	{
		if(_isWaitingInteraction && isInteractable)
		{
			_isWaitingInteraction = false;
			AnimateOut();
		}
	}
	*/
}

