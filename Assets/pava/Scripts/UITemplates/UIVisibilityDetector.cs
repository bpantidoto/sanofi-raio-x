﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVisibilityDetector : MonoBehaviour 
{
	private RectTransform[] _targets;
	private RectTransform _rectTransform;
	private CanvasGroup _canvasGroup;

	public UIAnimation animator;

	private string _xpos = "_xpos";
	private string _ypos = "_ypos";
	private string _zpos = "_zpos";

	public bool isVisible = false;
	
	private void Awake() 
	{
		_rectTransform = gameObject.GetComponent<RectTransform>();
		_targets = gameObject.GetComponentsInChildren<RectTransform>();
		_canvasGroup = gameObject.GetComponent<CanvasGroup>();

		_canvasGroup.alpha = 0;

#if UNITY_EDITOR
		_canvasGroup.alpha = 1;
#endif
		ApplyInitialPos();
	}

	public void SetInitialPosition()
	{
		string myName = gameObject.name;
		PlayerPrefs.SetFloat(myName + _xpos, _rectTransform.localPosition.x);
	}

	public void ApplyInitialPos()
	{
		string myName = gameObject.name;
		float xPos = PlayerPrefs.GetFloat(myName + _xpos);
		Vector3 initialPosition = new Vector3(xPos, _rectTransform.localPosition.y, _rectTransform.localPosition.z);
		_rectTransform.localPosition = initialPosition;
	}

	private void Update() 
	{
		CheckVisibility();
	}

	private void CheckVisibility()
	{
		bool atLeastOneVisible = false;
		foreach(RectTransform current in _targets)
		{
			if(current.IsVisibleFrom(Camera.main))
			{
				atLeastOneVisible = true;
			}
		}	
		
		isVisible = atLeastOneVisible;
		if(isVisible)
		{
			if(!animator.isVisible)
			{
				animator.AnimateIn();
			}
		}else{
			if(animator.isVisible)
			{
				animator.AnimateOut();
			}
		}
	}
}
