﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Linq;

public class UIPopupChoseProducts : UIPopup 
{
    private List<Image> _buttons;
    private Sprite[] _sprites;
    private Image _openerButton;
    private int _correctAlternative;

    public bool isCorrect = false;

    public void SetOpenerButtonAndCorrectAlternative(Image button, int correctAlternative)
    {
        _openerButton = button;
        _correctAlternative = correctAlternative;
        isCorrect = false;
    }

    public void AssignProductImages(Sprite[] sprites)
    {
        _sprites = sprites;
        _buttons = new List<Image>();
        
        int count = 0;
        for(int i = 0; i < transform.childCount; i++)
        {
            Image current = transform.GetChild(i).GetComponent<Image>();
            if(current != null)
            {
                Button button = current.GetComponent<Button>();
                button.interactable = true;
                _buttons.Add(current);
                _buttons[count].sprite = _sprites[count];
                count++;
            }
        }
    }

	public void ChoseProduct(int index)
    {
        _openerButton.sprite = _sprites[index];

        if(_correctAlternative == index)
        {
            isCorrect = true;
            //Debug.Log("CORRECT!");
            //TODO: ADD POINTS
        }else{
            isCorrect = false;
            //Debug.Log("NOT TODAY!");
            //TODO: SUBTRACT POINTS
        }

        foreach(Image current in _buttons)
        {
            current.GetComponent<Button>().interactable = false;
        }

        ClosePopup();
    }
}
