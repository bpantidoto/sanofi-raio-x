﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UIButtonOpenDoubleQuestion : MonoBehaviour, IPointerClickHandler
{	
	public ExercicePoints exercisePoints;
	public GameObject popup;
	public int[] questionIndexes;

	private CanvasGroup _canvasGroup;
	private UIPopupQuestion _uiPopup;
	private Image _button;

	private bool isInteractable = true;

	private QuestionsManager _questionsManager;

	private int _currentQuestion = 0;

	private UITouchlocker _touchlocker;

	public CustomEvent onQuestionComplete;

	private PointsManager _pointsManager;

	[HideInInspector]
	public int tries = 0;

	private void Awake() 
	{
		popup.SetActive(false);
		_uiPopup = popup.GetComponent<UIPopupQuestion>();
		_button = GetComponent<Image>();
		_canvasGroup = GetComponent<CanvasGroup>();
		_pointsManager = GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>();
		_questionsManager = GameObject.FindGameObjectWithTag("QuestionsManager").GetComponent<QuestionsManager>();
		_touchlocker = GameObject.FindGameObjectWithTag("Touchlocker").GetComponent<UITouchlocker>();
		_canvasGroup.alpha = 0;

		#if UNITY_EDITOR
			_canvasGroup.alpha = 0.5f;
		#endif
	}

	public void OnPointerClick(PointerEventData data)
    {
		if(isInteractable)
		{
			OpenQuestion();
		}
    }

	private void OpenQuestion()
	{
		popup.SetActive(true);

		_uiPopup.onOpen.AddListener(delegate{OnPopupOpen();});
		_uiPopup.onClose.AddListener(delegate{OnPopupClose();});
		_uiPopup.onBeginOpen.AddListener(delegate{OnPopupBeginOpen();});
		_uiPopup.onBeginClose.AddListener(delegate{OnPopupBeginClose();});

		_uiPopup.AssignQuestion(_questionsManager.questions[questionIndexes[_currentQuestion]]);
		_uiPopup.OpenPopup();
	}

	private void OnPopupClose()
	{
		_uiPopup.onOpen.RemoveAllListeners();
		_uiPopup.onClose.RemoveAllListeners();
		_uiPopup.onBeginOpen.RemoveAllListeners();
		_uiPopup.onBeginClose.RemoveAllListeners();
		popup.SetActive(false);
		isInteractable = false;

		DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();
		databaseManager.InsertRelatory (_questionsManager.questions[questionIndexes[_currentQuestion]].title, 
										_uiPopup.isCorrect ? 1 : 0,
										_questionsManager.questions[questionIndexes[_currentQuestion]].answers[_uiPopup.chosenAnswer]);

		if(!_uiPopup.isCorrect)
		{
			tries++;
			_pointsManager.SubtractPoints(exercisePoints.wrongAnswerCost);
			exercisePoints.AddWrongAnswer();
		}else{
			_pointsManager.AddPoints(exercisePoints.exerciseValue);
			exercisePoints.AddRightAnswer();
		}

		_currentQuestion++;
		if(stillHaveQuestions())
		{
			OpenQuestion();
		}else
		{
			_touchlocker.EnableTouch();
			onQuestionComplete.Invoke();
			gameObject.SetActive(false);
		}
	}

	private void OnPopupOpen()
	{
		_touchlocker.EnableTouch();
	}

	private void OnPopupBeginOpen()
	{

	}

	private void OnPopupBeginClose()
	{
		if(stillHaveQuestions())
		{
			_touchlocker.DisableTouch();
		}
	}

	private bool stillHaveQuestions()
	{
		return (_currentQuestion < questionIndexes.Length);
	}
}


