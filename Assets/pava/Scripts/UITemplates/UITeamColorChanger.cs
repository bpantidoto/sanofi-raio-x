﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITeamColorChanger : MonoBehaviour 
{
	private Image _image;

	private void Awake()
	{
		_image = GetComponent<Image>();

		GameObject.FindObjectOfType<TeamManager>().OnSetTeam.AddListener(delegate{ChangeTeamColor();});
	}
	
	private void ChangeTeamColor()
	{
		_image.color = GameObject.FindObjectOfType<TeamManager>().teamColor;
	}
}
