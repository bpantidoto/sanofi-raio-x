﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPositionConfigurator : MonoBehaviour 
{
	public RectTransform panelContents;
	public Dropdown dropdownSelectContent;
	public RectTransform[] contents;
	

	private RectTransform _target;

	private void Awake()
	{
		dropdownSelectContent.ClearOptions();

		foreach(RectTransform current in contents)
		{
			Dropdown.OptionData data = new Dropdown.OptionData();
			data.text = current.name;
			dropdownSelectContent.options.Add(data);
		}
	}

	public void Save()
	{
		Debug.Log("Save");
	}

	public void SetPosition()
	{
		if(_target != null)
		{
			Debug.Log("SetPosition");
			float xPos = (panelContents.localPosition.x*-1);
			_target.localPosition = new Vector3(xPos, _target.localPosition.y, _target.localPosition.z);
			_target.GetComponent<UIVisibilityDetector>().SetInitialPosition();
		}
	}

	public void OnDropdownValueChange()
	{
		Debug.Log("OnDropdownValueChange");
		int value = dropdownSelectContent.value;
		string contentName = dropdownSelectContent.options[value].text;
		RectTransform target = GameObject.Find(contentName).GetComponent<RectTransform>();
		_target = target;
	}
}
