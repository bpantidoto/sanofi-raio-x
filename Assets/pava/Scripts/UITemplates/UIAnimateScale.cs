﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIAnimateScale : MonoBehaviour 
{
	public float tweeningTime = 0.3f;
	public float delay = 0f;

	public CustomEvent onBeginAnimateIn;
	public CustomEvent onBeginAnimateOut;
	public CustomEvent onEndAnimateIn;
	public CustomEvent onEndAnimateOut;

	public Ease easingIn;
	public Ease easingOut;

	public void SetupScale()
	{
		transform.localScale = Vector3.zero;
	}

	public void AnimateIn()
	{
		onBeginAnimateIn.Invoke();
		transform.DOScale(new Vector3(1f,1f,1f), tweeningTime).SetEase(easingIn).SetDelay(delay).OnComplete(delegate{
			onEndAnimateIn.Invoke();
		});
	}

	public void AnimateOut()
	{
		onBeginAnimateOut.Invoke();
		transform.DOScale(new Vector3(0f,0f,0f), tweeningTime).SetEase(easingOut).SetDelay(delay).OnComplete(delegate{
			onEndAnimateOut.Invoke();
		});
	}
}
