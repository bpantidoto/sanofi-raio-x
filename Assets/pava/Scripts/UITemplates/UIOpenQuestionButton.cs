﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UIOpenQuestionButton : MonoBehaviour, IPointerClickHandler
{	
	public ExercicePoints exercisePoints;
	public GameObject popup;
	public int questionIndex;

	private CanvasGroup _canvasGroup;
	private UIPopupQuestion _uiPopup;
	private Image _button;

	private bool isInteractable = true;

	private QuestionsManager _questionsManager;

	public CustomEvent onQuestionComplete;

	private PointsManager _pointsManager;

	[HideInInspector]
	public int tries = 0;

	private void Awake() 
	{
		popup.SetActive(false);
		_uiPopup = popup.GetComponent<UIPopupQuestion>();
		_button = GetComponent<Image>();
		_canvasGroup = GetComponent<CanvasGroup>();
		_pointsManager = GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>();
		_questionsManager = GameObject.FindGameObjectWithTag("QuestionsManager").GetComponent<QuestionsManager>();
		_canvasGroup.alpha = 0;

		#if UNITY_EDITOR
			_canvasGroup.alpha = 0.5f;
		#endif
	}

	public void OnPointerClick(PointerEventData data)
    {
		if(isInteractable)
		{
			popup.SetActive(true);

			_uiPopup.onOpen.AddListener(delegate{OnPopupOpen();});
			_uiPopup.onClose.AddListener(delegate{OnPopupClose();});

			_uiPopup.AssignQuestion(_questionsManager.questions[questionIndex]);
			_uiPopup.OpenPopup();
		}
    }

	private void OnPopupClose()
	{
		_uiPopup.onOpen.RemoveAllListeners();
		_uiPopup.onClose.RemoveAllListeners();
		popup.SetActive(false);

		//TODO: CHECK IF THE PLAYER MAY ANSWER THE QUESTION AGAIN IF HE PREVIOULSY ANSWERED WRONG
		isInteractable = false;

		DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();
		databaseManager.InsertRelatory (_questionsManager.questions[questionIndex].title, _uiPopup.isCorrect ? 1 : 0,_questionsManager.questions[questionIndex].answers[_uiPopup.chosenAnswer]);

		if(!_uiPopup.isCorrect)
		{
			tries++;
			_pointsManager.SubtractPoints(exercisePoints.wrongAnswerCost);
			exercisePoints.AddWrongAnswer();
		}else{
			_pointsManager.AddPoints(exercisePoints.exerciseValue);
			exercisePoints.AddRightAnswer();
		}

		onQuestionComplete.Invoke();
		gameObject.SetActive(false);
	}

	private void OnPopupOpen()
	{

	}
}

