﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIAnimation : MonoBehaviour 
{
    private CanvasGroup _canvasGroup;
    private RectTransform _rectTransform;

    [System.NonSerialized]
    public bool isVisible = false;

    public CustomEvent onAnimateInComplete;
    public CustomEvent onAnimateOutComplete;
    
    private Vector3 _startPosition;
    private Vector3 _visiblePosition;

    public bool ignorePosition = false;

    public float delayIn = 0;
    public float delayOut = 0;

    public float startAlpha = 0;


    private bool _isAnimatingIn = false;
    private bool _isAnimatingOut = false;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _rectTransform = GetComponent<RectTransform>();

        if(!ignorePosition)
        {
            _startPosition = _rectTransform.localPosition;
            _visiblePosition = new Vector3(0, _startPosition.y, _startPosition.z);
        }

        _canvasGroup.alpha = startAlpha;
    }

    public void AnimateIn()
    {
        if(!_isAnimatingIn && !isVisible)
        {
            _isAnimatingIn = true;
            if(!ignorePosition)
            {
                _rectTransform.localPosition = _visiblePosition;
            }
            isVisible = true;
            _canvasGroup.alpha = 0f;
            _canvasGroup.DOFade(1f, 0.5f).SetDelay(delayIn).OnComplete(OnAnimateInComplete);
        }
    }

    public void AnimateOut()
    {
        if(!_isAnimatingOut && isVisible)
        {
            _isAnimatingOut = true;
            _canvasGroup.DOFade(0f, 0.5f).SetDelay(delayOut).OnComplete(OnAnimateOutComplete);
        }
    }

    private void OnAnimateInComplete()
    {
        onAnimateInComplete.Invoke();
        _isAnimatingIn = false;
    }

    private void OnAnimateOutComplete()
    {
        isVisible = false;
        if(!ignorePosition)
        {
            _rectTransform.localPosition = _startPosition;
        }
        onAnimateOutComplete.Invoke();
        _isAnimatingOut = false;
    }
}
