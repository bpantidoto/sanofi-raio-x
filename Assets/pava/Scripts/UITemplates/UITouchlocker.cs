﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITouchlocker : MonoBehaviour 
{
	private Image _image;

	private void Awake()
	{
		_image = GetComponent<Image>();
		EnableTouch();
	}
	
	public void EnableTouch()
	{
		_image.enabled = false;
	}

	public void DisableTouch()
	{
		_image.enabled = true;
	}
}
