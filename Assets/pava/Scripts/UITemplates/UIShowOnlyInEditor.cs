﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShowOnlyInEditor : MonoBehaviour 
{
	private CanvasGroup _canvasGroup;

	private void Start () 
	{
		_canvasGroup = GetComponent<CanvasGroup>();
		_canvasGroup.alpha = 0f;
		
#if UNITY_EDITOR
		_canvasGroup.alpha = 0.5f;
#endif
	}
}
