﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIPopup : MonoBehaviour, IPointerClickHandler
{
	private CanvasGroup _canvasGroup;

	public float tweenTime = 0.5f;
	public bool backgroundTouchToClose = true;

	public CustomEvent onOpen;
	public CustomEvent onClose;
	public CustomEvent onBeginOpen;
	public CustomEvent onBeginClose;

	private void Awake() 
	{
		_canvasGroup = GetComponent<CanvasGroup>();
		_canvasGroup.alpha = 0;
	}

	public void OpenPopup()
	{
		if(onBeginOpen != null)
		{
			onBeginOpen.Invoke();
		}
		_canvasGroup.DOFade(1f, tweenTime).OnComplete(OnAnimationOpenComplete);
	}

	public void ClosePopup()
	{
		if(onBeginClose != null)
		{
			onBeginClose.Invoke();
		}
		_canvasGroup.DOFade(0f, tweenTime).OnComplete(OnAnimationCloseComplete);
	}

	private void OnAnimationOpenComplete()
	{
		if(onOpen != null)
		{
			onOpen.Invoke();
		}
	}

	private void OnAnimationCloseComplete()
	{
		if(onClose != null)
		{
			onClose.Invoke();
		}
	}

	public void OnPointerClick(PointerEventData data)
	{
		if(backgroundTouchToClose)
		{
			ClosePopup();
		}
	}
}
