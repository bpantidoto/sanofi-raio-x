﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIChoseGroupPanel : MonoBehaviour 
{
	public Button[] alternatives;
	public Button next;

	private int _selected;

	public CustomEvent OnComplete;

	private void Awake() 
	{
		next.interactable = false; 
		
		//TODO: CHECK IF THIS IS NECESSARY HERE OR IN SOME PLACE ELSE.
		//my bet is on some place else.
		GameObject.FindObjectOfType<TeamManager>().ResetTeam();


		for(int i = 0; i< alternatives.Length; i++)
		{
			/* 
			ColorBlock colorblock = ColorBlock.defaultColorBlock;
			colorblock.normalColor = TeamManager.Instance.availableTeamColors[i];
			alternatives[i].colors = colorblock;
			*/

			//alternatives[i].GetComponent<Image>().color = TeamManager.Instance.availableTeamColors[i];
		}		
	}

	public void Select(int index)
	{
		Debug.Log("Chosen: "+index);
		foreach(Button current in alternatives)
		{
			current.interactable = true;
			current.GetComponent<UIChoseGroupButton>().Select(false);
		}
		alternatives[index].GetComponent<UIChoseGroupButton>().Select(true);
		alternatives[index].interactable = false;

		_selected = index;

		next.interactable = true;
		
		string teamName = alternatives[_selected].name.Replace("Button - ", "");
		TeamManager teamManager = GameObject.FindObjectOfType<TeamManager>();
		teamManager.SetTeamID(_selected);
		teamManager.SetTeamName(teamName);
	}

	public void Next()
	{
		gameObject.SetActive(false);
		OnComplete.Invoke();
		TeamManager teamManager = GameObject.FindObjectOfType<TeamManager>();
		teamManager.CreateDatabaseEntry();
	}
}
