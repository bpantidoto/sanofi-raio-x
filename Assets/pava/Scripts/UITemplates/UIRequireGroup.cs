﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRequireGroup : MonoBehaviour 
{
	public UIContentCompletionCheck choseGroup;
	private UIAnimation _animation;
	private UIAnimation _choseGroupAnimation;

	private void Awake()
	{
		_animation = GetComponent<UIAnimation>();
		_choseGroupAnimation = choseGroup.GetComponent<UIAnimation>();
	}

	private void Update()
	{
#if UNITY_EDITOR
		return;
#endif
		if(!choseGroup.isComplete && !_choseGroupAnimation.isVisible)
		{
			if(!_animation.isVisible)
			{
				_animation.AnimateIn();
			}
		}else{
			_animation.AnimateOut();
		}
	}	
}

