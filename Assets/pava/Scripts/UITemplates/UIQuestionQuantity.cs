﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestionQuantity : UIPopup 
{
	public Text productNameText;

	public Text corporativeQuantityText;
	public Text independentQuantityText;

	public GameObject buttonConfirm;
	public GameObject[] plusMinusButtons;

	private QuestionQuantityModel _question;

	private int _currentCorpQuantity = 0;
	private int _currentIndieQuantity = 0;

	public UIQuantityFeedback corporativeFeedback;
	public UIQuantityFeedback independentFeedback;	

	public float closePopupDelay = 3f;

	public void SetQuestionModel(QuestionQuantityModel model)
	{
		foreach(GameObject current in plusMinusButtons)
		{
			current.SetActive(true);	
		}

		buttonConfirm.SetActive(true);
		corporativeFeedback.gameObject.SetActive(false);
		independentFeedback.gameObject.SetActive(false);

		corporativeFeedback.Reset();
		independentFeedback.Reset();

		_question = model;

		_currentCorpQuantity = _question.corporativeQuantity;
		_currentIndieQuantity = _question.independentQuantity;

		SetTextProductName();
		SetTextQuantity();
	}

	private void SetTextProductName()
	{
		productNameText.text = _question.productName;
	}

	private void SetTextQuantity()
	{
		corporativeQuantityText.text = _currentCorpQuantity.ToString();
		independentQuantityText.text = _currentIndieQuantity.ToString();
	}

	private void SetFeedback()
	{
		corporativeFeedback.gameObject.SetActive(true);
		independentFeedback.gameObject.SetActive(true);

		corporativeFeedback.SetFeedback(_currentCorpQuantity == _question.correctCorporative, _question.correctCorporative);
		independentFeedback.SetFeedback(_currentIndieQuantity == _question.correctIndependent, _question.correctIndependent);

		StartCoroutine(ClosePopupWithDelay());
	}

	public bool GetCorporativeAnswer()
	{
		return (_currentCorpQuantity == _question.correctCorporative);
	}

	public bool GetIndependentAnswer()
	{
		return (_currentIndieQuantity == _question.correctIndependent);
	}

	public string GetDatabaseEntryCorporative()
	{
		return "Corporativo: "+_currentCorpQuantity;
	}

	public string GetDatabaseEntryIndependent()
	{
		return "Independente: "+_currentIndieQuantity;
	}

	private IEnumerator ClosePopupWithDelay()
	{
		yield return new WaitForSeconds(closePopupDelay);
		corporativeFeedback.Reset();
		independentFeedback.Reset();
		ClosePopup();
	}

	public void AddCorp()
	{
		_currentCorpQuantity++;
		SetTextQuantity();
	}

	public void RemoveCorp()
	{
		_currentCorpQuantity--;
		if(_currentCorpQuantity < 0)
		{
			_currentCorpQuantity = 0;
		}
		SetTextQuantity();
	}

	public void AddIndie()
	{
		_currentIndieQuantity++;
		SetTextQuantity();
	}

	public void RemoveIndie()
	{
		_currentIndieQuantity--;
		if(_currentIndieQuantity < 0)
		{
			_currentIndieQuantity = 0;
		}
		SetTextQuantity();
	}

	public void Confirm()
	{
		SetFeedback();
		buttonConfirm.SetActive(false);

		foreach(GameObject current in plusMinusButtons)
		{
			current.SetActive(false);	
		}
	}
}
