﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMatchContent : MonoBehaviour 
{
	public int correctAnswer = 5;
	public float stepValue = 0.1666f;
	
	public UINoDragScrollRect scrollRect;
	public Button leftButton;
	public Button rightButton;
	public Image feedback;

	public Color feedPos = Color.green;
	public Color feedNeg = Color.red;

	private int _childCount = 0;
	private int _currentStep = 0;

	private bool _isCorrect = false;
	public bool isCorrect {
		get {
			return _isCorrect;
		}
	}

	private void Awake() 
	{
		Transform content = scrollRect.transform.GetChild(0);
		_childCount = content.childCount;
		_currentStep = 1;
	}

	private void Start() 
	{
		CheckLimits();	
		CheckAnswer();
	}

	public void NavigateLeft()
	{
		_currentStep--;
		scrollRect.horizontalNormalizedPosition -= stepValue;
		CheckLimits();
		CheckAnswer();
	}

	public void NavigateRight()
	{
		_currentStep++;
		scrollRect.horizontalNormalizedPosition += stepValue;
		CheckLimits();
		CheckAnswer();
	}

	private void CheckLimits()
	{
		if(scrollRect.horizontalNormalizedPosition < (stepValue-0.0001f))
		{
			leftButton.interactable = false;
		}else{
			leftButton.interactable = true;
		}

		if(scrollRect.horizontalNormalizedPosition > (stepValue * (_childCount-1)))
		{
			rightButton.interactable = false;
		}else{
			rightButton.interactable = true;
		}
	}

	private bool CheckAnswer()
	{
		if(_currentStep == correctAnswer)
		{
			_isCorrect = true;
			feedback.color = feedPos;
		}else{
			_isCorrect = false;
			feedback.color = feedNeg;
		}

		return _isCorrect;
	}
}
