﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEndGame : MonoBehaviour 
{
	public UIContentCompletionCheck[] contents;
	public UIVisibilityDetector firstContent;

	private bool _shouldCheck = true;
	private bool _allComplete = false;

	public CustomEvent resetScene;

	private void Update()
	{
		if(_shouldCheck)
		{
			foreach(UIContentCompletionCheck current in contents)
			{
				if(!current.isComplete)
					return;
			}

			AllComplete();
		}

		if(_allComplete)
		{
			if(firstContent.isVisible)
			{
				resetScene.Invoke();
				return;
			}
		}
	}

	private void AllComplete()
	{
		_shouldCheck = false;
		gameObject.GetComponent<UIAnimation>().AnimateIn();
		/* 
		foreach(UIContentCompletionCheck current in contents)
		{
			current.gameObject.SetActive(false);
		}
		*/

		UITimer[] timers = GameObject.FindObjectsOfType<UITimer>();
		foreach(UITimer current in timers)
		{
			current.StopTimer();
		}

		_allComplete = true;
		DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();
		databaseManager.UpdateUser();
	}
}
