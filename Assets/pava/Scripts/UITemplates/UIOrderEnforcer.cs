﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOrderEnforcer : MonoBehaviour 
{
	public UIContentCompletionCheck[] order;
	public UIVisibilityDetector[] detectors;
	
	private int _currentContent = 0;

	private UIAnimation _animation;

	public bool debug = true;

	private void Start()
	{
		_animation = GetComponent<UIAnimation>();
	}

	public void Update()
	{
		#if UNITY_EDITOR
		if(debug)
			return;
		#endif
		_currentContent = 0;
		for(int i = 0; i < order.Length; i++)
		{
			if(order[i].isComplete)
			{
				_currentContent++;
			}
		}

		if(_currentContent < detectors.Length)
		{
			if(!detectors[_currentContent].isVisible && detectors[_currentContent].transform.position.x < Camera.main.transform.position.x)
			{
				_animation.AnimateIn();
			}else{
				_animation.AnimateOut();
			}
		}
	}
}
