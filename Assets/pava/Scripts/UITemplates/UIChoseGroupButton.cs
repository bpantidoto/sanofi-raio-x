﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIChoseGroupButton : MonoBehaviour 
{
	public GameObject selection;

	private RectTransform _rectTransform;
	private RectTransform _selectionRect;

	private void Awake()
	{
		Select(false, false);
		_rectTransform = GetComponent<RectTransform>();
		_selectionRect = selection.GetComponent<RectTransform>();
	}

	public void Select(bool isActive, bool animate=true)
	{
		if(animate)
		{
			if(isActive)
			{
				selection.SetActive(isActive);
				_selectionRect.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0);

				Sequence mySequence = DOTween.Sequence();

				mySequence.Append(_rectTransform.DOScale(new Vector3(0.7f, 0.7f, 0.7f), 0.2f).SetEase(Ease.OutCubic));
				//mySequence.AppendInterval(0.1f);
				mySequence.Append(_rectTransform.DOScale(new Vector3(1f, 1f, 1f), 0.2f).SetEase(Ease.OutCubic));
				mySequence.Append(_selectionRect.DOScale(new Vector3(1f, 1f, 1f), 0.2f).SetEase(Ease.OutCubic));
				
			}else{
				selection.SetActive(isActive);
			}
		}else{
			selection.SetActive(isActive);
		}
	}
}
