﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDragNDropVerifier : MonoBehaviour 
{
	public bool isDragNDropComplete = false;
	public CustomEvent onAllComplete;

	private UIDropIdentifier[] _drops;
	private UIDragIdentifier[] _drags;

	private bool _hasDispatchedEvent = false;

	public int totalTries = 0;

	private void Awake() 
	{
		_drops = GetComponentsInChildren<UIDropIdentifier>();
		_drags = GetComponentsInChildren<UIDragIdentifier>();
	}

	private void Update()
	{
		bool allComplete = true;
		totalTries = 0;
		foreach(UIDropIdentifier current in _drops)
		{
			if(!current.isComplete)
			{
				allComplete = false;
			}
			totalTries += current.tries;
		}

		if(allComplete && !_hasDispatchedEvent)
		{
			onAllComplete.Invoke();
			_hasDispatchedEvent = true;
			isDragNDropComplete = true;
			DisableAll();
			return;
		}
	}

	private void DisableAll()
	{
		foreach(UIDragIdentifier current in _drags)
		{
			current.ToggleDrag(false);
		}
	}
}
