﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UIButtonAction : MonoBehaviour, IPointerClickHandler
{	
	private bool isInteractable = true;

	public CustomEvent onClick;

	public void OnPointerClick(PointerEventData data)
    {
		if(isInteractable)
		{
			onClick.Invoke();
		}
    }
}

