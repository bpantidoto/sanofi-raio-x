﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler  
{
	public static GameObject itemBeingDragged;
	private Vector3 _startPosition;

	private RectTransform _rectTransform;
	private RectTransform _parentRect;

	public CustomEvent onBeginDrag;
	public CustomEvent onEndDrag;

	public float tweenDuration = 0.5f;

	private bool _canDrag = true;


	public Color negativeFeedback = Color.red;
	public Color positiveFeedback = Color.green;
	public Color neutral = Color.white;

	public void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		_parentRect = transform.parent.GetComponent<RectTransform>();
	}

	private void Start() 
	{
		_startPosition = _rectTransform.localPosition;
	}

#region IBeginDragHandler implementation
	public void OnBeginDrag (PointerEventData eventData)
	{
		if(!_canDrag)
			return;

		itemBeingDragged = gameObject;

		GetComponent<Image> ().raycastTarget = false;
		transform.SetAsLastSibling();

		if(onBeginDrag != null)
		{
			onBeginDrag.Invoke();
		}
	}
#endregion

#region IDragHandler implementation
	public void OnDrag (PointerEventData eventData)
	{
		if(!_canDrag)
			return;			

		transform.localPosition += (Vector3)eventData.delta;
		float xPos = transform.localPosition.x;
		float yPos = transform.localPosition.y;
		
		if(xPos - (_rectTransform.sizeDelta.x/2) < -(_parentRect.sizeDelta.x/2))
		{
			xPos = - (_parentRect.sizeDelta.x/2) + (_rectTransform.sizeDelta.x/2);
		}

		if(yPos - (_rectTransform.sizeDelta.y/2) < -(_parentRect.sizeDelta.y/2))
		{
			yPos = - (_parentRect.sizeDelta.y/2) + (_rectTransform.sizeDelta.y/2);
		}

		if(xPos + (_rectTransform.sizeDelta.x/2) > (_parentRect.sizeDelta.x/2))
		{
			xPos = (_parentRect.sizeDelta.x/2) - (_rectTransform.sizeDelta.x/2);
		}

		if(yPos + (_rectTransform.sizeDelta.y/2) > (_parentRect.sizeDelta.y/2))
		{
			yPos = (_parentRect.sizeDelta.y/2) - (_rectTransform.sizeDelta.y/2);
		}
		Vector2 newPos = new Vector2(xPos, yPos);
		transform.localPosition = newPos;
	}
#endregion

#region IEndDragHandler implementation
	public void OnEndDrag (PointerEventData eventData)
	{
		if(!_canDrag)
			return;

		if(onEndDrag != null)
		{
			onEndDrag.Invoke();
		}

		GetComponent<Image> ().raycastTarget = true;
		itemBeingDragged = null;
	}
#endregion

	public void ResetPosition()
	{
		_rectTransform.DOLocalMove(_startPosition, tweenDuration).OnComplete(delegate{
			_rectTransform.GetComponent<Image>().DOColor(negativeFeedback, 0.3f).SetLoops(2,LoopType.Yoyo).OnComplete(delegate{
				_rectTransform.GetComponent<Image>().DOColor(neutral, 0);
			});
		});
	}

	public void AssignPosition(Vector2 position)
	{
		_rectTransform.DOLocalMove(new Vector3(position.x, position.y, 0), tweenDuration).OnComplete(delegate {
			_rectTransform.GetComponent<Image>().DOColor(positiveFeedback, 0.3f).SetLoops(2,LoopType.Yoyo).OnComplete(delegate{
				_rectTransform.GetComponent<Image>().DOColor(neutral, 0);
			});
		});
	}

	public void ToggleDrag(bool canDrag)
	{
		_canDrag = canDrag;
	}

	public void CancelTweens()
	{
		DOTween.KillAll(false);
	}
}
