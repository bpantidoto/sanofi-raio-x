﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFeedbackQuantity : MonoBehaviour 
{
	public UIPopupQuestionTripleFeedback parentPopup;

	public Text positiveText;
	public Text negativeText;
	public Text productName;
	public Text quantityText;
	public GameObject positiveImage;
	public GameObject negativeImage;

	public string defaultText = "Resposta errada.\nO correto é {0}";


	public bool lastAnswerIsCorrect = false;

	public void SetFeedback(bool isPositive, QuestionTrueOrFalseModel model, int quantityAnswered=0)
	{
		productName.text = model.productName;
		lastAnswerIsCorrect = isPositive;
		
		if(isPositive)
		{
			positiveText.gameObject.SetActive(true);
			negativeText.gameObject.SetActive(false);

			positiveImage.SetActive(true);
			negativeImage.SetActive(false);

			quantityText.text = model.rightQuantity.ToString();
		}else{
			positiveText.gameObject.SetActive(false);
			negativeText.gameObject.SetActive(true);

			positiveImage.SetActive(false);
			negativeImage.SetActive(true);

			negativeText.text = string.Format(defaultText, model.rightQuantity);

			if(quantityAnswered > 0)
			{
				quantityText.text = quantityAnswered.ToString();
			}else{
				quantityText.text = model.wrongQuantity.ToString();
			}
		}
		StartCoroutine(ClosePopup());
	}

	private IEnumerator ClosePopup()
	{
		yield return new WaitForSeconds(3f);
		parentPopup.Close();
	}
}
