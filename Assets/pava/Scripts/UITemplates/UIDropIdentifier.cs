﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDropIdentifier : UIDropHandler 
{
	public ExercicePoints exercisePoints;
	public int ID = 0;
	public bool shouldLockDragOnDropComplete = true;
	public bool resetPositionOnWrongAnswer = true;

	private bool _isComplete = false;
	public bool isComplete
	{
		get{
			return _isComplete;
		}
	}

	public Transform feedbacks;

	[HideInInspector]
	public int tries = 0;

	private PointsManager _pointsManager;
	
	private void Awake()
	{
		_pointsManager = GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>();
		onDrop.AddListener(delegate{Drop();});

		if(feedbacks != null)
		{
			for(int i = 0; i < feedbacks.childCount; i++)
			{
				feedbacks.GetChild(i).gameObject.SetActive(false);
			}
		}
	}

	private void Drop()
	{
		UIDragIdentifier item;
		if(UIDragIdentifier.itemBeingDragged != null)
		{
			item = UIDragIdentifier.itemBeingDragged.GetComponent<UIDragIdentifier>();
			item.CancelTweens();

			DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();

			if(item.ID == ID)
			{
				item.shouldReset = false;
				_isComplete = true;
				item.AssignPosition(GetComponent<RectTransform>().localPosition);
				item.ToggleDrag(false);
				databaseManager.InsertRelatory (item.exerciseIdentifier, 1, item.alternativeIdentifier);
				_pointsManager.AddPoints(exercisePoints.exerciseValue);
				exercisePoints.AddRightAnswer();
			}else{
				databaseManager.InsertRelatory (item.exerciseIdentifier, 0, item.alternativeIdentifier);
				_pointsManager.SubtractPoints(exercisePoints.wrongAnswerCost);
				exercisePoints.AddWrongAnswer();
				tries ++;

				if(resetPositionOnWrongAnswer)
				{
					item.ResetPosition();
				}

				if(feedbacks != null)
				{
					GameObject currentFeedback;
					item.ToggleDrag(false);
					if(item.ID > 1)
					{
						currentFeedback = feedbacks.GetChild(item.ID-1).gameObject;
					}else{
						currentFeedback = feedbacks.GetChild(item.ID).gameObject;
					}
					currentFeedback.SetActive(true);
					UIAnimateScale animator = currentFeedback.GetComponent<UIAnimateScale>();
					animator.SetupScale();
					animator.AnimateIn();
				}
			}
		}
	}
}
