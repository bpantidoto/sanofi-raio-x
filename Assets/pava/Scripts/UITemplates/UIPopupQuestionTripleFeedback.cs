﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopupQuestionTripleFeedback : UIPopup 
{
	private QuestionTrueOrFalseModel _currentQuestion;

	public UIQuestionTrueOrFalse question;
	public UITypeQuantity popupChoseQuantity;
    public UIFeedbackQuantity feedbackQuantity;

	public void SetQuestionModel(QuestionTrueOrFalseModel model)
	{
		_currentQuestion = model;

		question.gameObject.SetActive(true);
		popupChoseQuantity.gameObject.SetActive(false);
		feedbackQuantity.gameObject.SetActive(false);

		question.SetQuestion(_currentQuestion);
	}

	public void Close()
	{
		ClosePopup();
	}
}
