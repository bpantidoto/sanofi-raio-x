﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContentCompletionCheck : MonoBehaviour 
{
	public bool isComplete = false;

	public int totalExerciseSteps = 0;
	private int _currentStep = 0;

	public CustomEvent onExerciseComplete;

	public void SetComplete()
	{
		isComplete = true;
	}

	public void AddExerciseStep()
	{
		_currentStep++;
		Debug.Log(_currentStep+"_"+totalExerciseSteps);
		if(_currentStep == totalExerciseSteps && totalExerciseSteps > 0)
		{
			Debug.Log("HERE2");
			onExerciseComplete.Invoke();
		}
	}
}
