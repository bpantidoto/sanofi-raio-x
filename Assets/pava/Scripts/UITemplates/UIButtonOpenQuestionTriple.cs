﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UIButtonOpenQuestionTriple : MonoBehaviour, IPointerClickHandler
{
	public ExercicePoints exercisePoints;
	public UIQuestionQuantity popup;
	public QuestionQuantityModel[] questions;
	private int _currentQuestion = 0;

	private bool isInteractable = true;
	private UITouchlocker _touchlocker;

	public CustomEvent onQuestionComplete;

	public bool asnweredCorrectly = false;

	private PointsManager _pointsManager;

	private void Awake() 
	{
		_touchlocker = GameObject.FindGameObjectWithTag("Touchlocker").GetComponent<UITouchlocker>();
		_pointsManager = GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>();
		
	}

	public void OnPointerClick(PointerEventData data)
    {
		if(isInteractable)
		{
			OpenQuestion();
		}
    }

	private void OpenQuestion()
	{
		//TODO: SERIES OF QUESTIONS
		QuestionQuantityModel question = questions[_currentQuestion];

		popup.gameObject.SetActive(true);

		popup.onOpen.AddListener(delegate{OnPopupOpen();});
		popup.onClose.AddListener(delegate{OnPopupClose();});
		popup.onBeginOpen.AddListener(delegate{OnPopupBeginOpen();});
		popup.onBeginClose.AddListener(delegate{OnPopupBeginClose();});
		
		popup.SetQuestionModel(question);
		popup.OpenPopup();
	}

	private void OnPopupClose()
	{
		popup.onOpen.RemoveAllListeners();
		popup.onClose.RemoveAllListeners();
		popup.onBeginOpen.RemoveAllListeners();
		popup.onBeginClose.RemoveAllListeners();

		DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();
		databaseManager.InsertRelatory (questions[_currentQuestion].productName, popup.GetCorporativeAnswer() ? 1 : 0, popup.GetDatabaseEntryCorporative());
		databaseManager.InsertRelatory (questions[_currentQuestion].productName, popup.GetIndependentAnswer() ? 1 : 0, popup.GetDatabaseEntryIndependent());
		
		if(popup.GetCorporativeAnswer())
		{
			_pointsManager.AddPoints(exercisePoints.exerciseValue);
			exercisePoints.AddRightAnswer();
		}else{
			_pointsManager.SubtractPoints(exercisePoints.wrongAnswerCost);
			exercisePoints.AddWrongAnswer();
		}

		if(popup.GetIndependentAnswer())
		{
			_pointsManager.AddPoints(exercisePoints.exerciseValue);
			exercisePoints.AddRightAnswer();
		}else{
			_pointsManager.SubtractPoints(exercisePoints.wrongAnswerCost);
			exercisePoints.AddWrongAnswer();
		}

		popup.gameObject.SetActive(false);
		isInteractable = false;

		_currentQuestion++;
		if(stillHaveQuestions())
		{
			OpenQuestion();
		}else
		{
			_touchlocker.EnableTouch();
			onQuestionComplete.Invoke();
			gameObject.SetActive(false);
		}
	}

	private void OnPopupOpen()
	{
		_touchlocker.EnableTouch();
	}

	private void OnPopupBeginOpen()
	{

	}

	private void OnPopupBeginClose()
	{
		if(stillHaveQuestions())
		{
			_touchlocker.DisableTouch();
		}
	}

	private bool stillHaveQuestions()
	{
		return (_currentQuestion < questions.Length);
	}
}

[System.Serializable]
public class QuestionTrueOrFalseModel
{
	public string questionTitle = "";
	public string productName = "";
	public int questionQuantity;
	public int rightQuantity;
	public int wrongQuantity;
	public bool answer;
}

[System.Serializable]
public class QuestionQuantityModel
{
	public string productName = "";
	public int corporativeQuantity;
	public int independentQuantity;
	public int correctCorporative;
	public int correctIndependent;
}
