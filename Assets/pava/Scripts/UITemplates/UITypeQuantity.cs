﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITypeQuantity : MonoBehaviour 
{
	public InputField inputField;
	public UIFeedbackQuantity feedbackQuantity;

	private QuestionTrueOrFalseModel _question;

	private int _currentQuantity = 0;

	public void SetQuestion(QuestionTrueOrFalseModel model)
	{
		_question = model;
		inputField.text = "";
		_currentQuantity = 0;
		SetTextValue();
	}

	public void Confirm()
	{
		if(inputField.text != "" && _currentQuantity > 0)
		{
			int chosenValue = int.Parse(inputField.text);
			gameObject.SetActive(false);
			feedbackQuantity.gameObject.SetActive(true);

			if(chosenValue == _question.rightQuantity)
			{
				feedbackQuantity.SetFeedback(true, _question);
			}else{
				feedbackQuantity.SetFeedback(false, _question, chosenValue);
			}
		}
	}

	public void Add()
	{
		_currentQuantity++;
		SetTextValue();
	}

	public void Remove()
	{
		_currentQuantity--;
		SetTextValue();
	}

	private void SetTextValue()
	{
		inputField.text = _currentQuantity.ToString();
	}
}
