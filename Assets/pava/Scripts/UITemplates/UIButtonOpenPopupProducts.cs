﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UIButtonOpenPopupProducts : MonoBehaviour, IPointerClickHandler
{	
	public GameObject popup;
	public UIFeedback feedback;

	private CanvasGroup _canvasGroup;
	private UIPopupChoseProducts _uiPopup;
	private Image _button;

	public int correctAnswer = 0;
	public Sprite[] sprites;

	private bool isInteractable = true;

	private void Awake() 
	{
		popup.SetActive(false);
		_uiPopup = popup.GetComponent<UIPopupChoseProducts>();
		_button = GetComponent<Image>();
	}

	public void OnPointerClick(PointerEventData data)
    {
		if(isInteractable)
		{
			popup.SetActive(true);

			_uiPopup.onOpen.AddListener(delegate{OnPopupOpen();});
			_uiPopup.onClose.AddListener(delegate{OnPopupClose();});

			_uiPopup.SetOpenerButtonAndCorrectAlternative(_button, correctAnswer);
			_uiPopup.AssignProductImages(sprites);
			_uiPopup.OpenPopup();
		}
    }

	private void OnPopupClose()
	{
		_uiPopup.onClose.RemoveAllListeners();
		popup.SetActive(false);

		feedback.gameObject.SetActive(true);


		//feedback.SetFeedback(_uiPopup.isCorrect);
		
		feedback.AnimateIn();

		//TODO: REMOVE/ADD POINTS
		if(_uiPopup.isCorrect)
		{
			isInteractable = false;
			GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>().AddPoints(100);
		}else{
			GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>().SubtractPoints(100);
		}
	}

	private void OnPopupOpen()
	{

	}
}

