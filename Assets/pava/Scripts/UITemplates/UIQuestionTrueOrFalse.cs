﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestionTrueOrFalse : MonoBehaviour 
{
    public Text productNameText;
    public Text quantityText;

    private QuestionTrueOrFalseModel _question;

    public UITypeQuantity popupChoseQuantity;
    public UIFeedbackQuantity feedbackQuantity;

	public void SetQuestion(QuestionTrueOrFalseModel model)
    {
        _question = model;
        productNameText.text = model.productName;
        quantityText.text = string.Format(quantityText.text, model.questionQuantity);
    }

    public void ChoseAnswer(bool isTrue)
    {
        gameObject.SetActive(false);

        if(isTrue && _question.answer)
        {
            feedbackQuantity.gameObject.SetActive(true);
            feedbackQuantity.SetFeedback(true, _question);
        }

        if(!isTrue && !_question.answer)
        {
            popupChoseQuantity.gameObject.SetActive(true);
            popupChoseQuantity.SetQuestion(_question);
        }

        if(!isTrue && _question.answer)
        {
            feedbackQuantity.gameObject.SetActive(true);
            feedbackQuantity.SetFeedback(false, _question);
        }

        if(isTrue && !_question.answer)
        {
            feedbackQuantity.gameObject.SetActive(true);
            feedbackQuantity.SetFeedback(false, _question);
        }
    }
}
