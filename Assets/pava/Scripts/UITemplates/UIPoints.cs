﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPoints : MonoBehaviour 
{
	private Text _textfield;

	public string defaultLabel = "Pontos: {0}";

	private int _points;
	public int points {
		get {
			int pts = GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>().points;
			return pts;
		}
	}

	private void Awake() 
	{
		_textfield = GetComponent<Text>();
	}

	private void Update() 
	{
		_textfield.text = string.Format(defaultLabel, points.ToString());
	}
}