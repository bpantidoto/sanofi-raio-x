﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIProductsDisplayer : MonoBehaviour 
{
	public int maxQuestions = 5;
	public float delay = 2f;
	private int _currentQuestion = 0;

	public CanvasGroup wrongProducts;
	public CanvasGroup correctProducts;

	public CustomEvent onAllComplete;

	public Color positiveFeedback = Color.green;

	private void Awake()
	{
		correctProducts.alpha = 0;
	}

	public void QuestionAnswered()
	{
		_currentQuestion++;
		if(_currentQuestion == maxQuestions)
		{
			ApplyFeedback();
		}
	}

	private void ApplyFeedback()
	{
		StartCoroutine(Feedback());
	}

	private IEnumerator Feedback()
	{
		yield return new WaitForSeconds(delay);
		wrongProducts.DOFade(0f, 0.5f);
		correctProducts.DOFade(3f, 0.5f).OnComplete(delegate{
			correctProducts.GetComponent<Image>().DOColor(positiveFeedback, 0.3f).SetLoops(2,LoopType.Yoyo).OnComplete(delegate {
				onAllComplete.Invoke();
			});
		});
	}
}

