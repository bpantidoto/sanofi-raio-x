﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
	public bool autoload = false;
	public string autoloadSceneName = "EncoderActualValue";

	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	public void Awake()
	{
		if(autoload)
		{
			StartCoroutine(AutoLoad());
		}
	}

	private IEnumerator AutoLoad()
	{
		yield return new WaitForEndOfFrame();
		SceneManager.LoadScene(autoloadSceneName);	
	}
}
