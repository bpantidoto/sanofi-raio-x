﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;
using UnityEngine.UI;

public class EncoderReader : MonoBehaviour
{
    public bool DebugMode;    
    public Text DebugText;

    public string portName;
    private int threshold;

    private SerialPort serialPort;
    private bool portOpen = false;

    public int currentValue = 0;
    public int lastValue = 0;

    public bool isMovingRight = false;
    public bool isMovingLeft = false;
    public bool isStoped = true;
    public bool isReady = false;

    public float velocity = 0f;
            
    private void Awake()
    {
        Input.multiTouchEnabled = false; 
        string port = "COM3";
        #if UNITY_EDITOR
            port = "COM23";
        #endif

        if(port != "")
        {
            portName = port;
        }

        serialPort = new SerialPort(portName)
        {
            DtrEnable = true
        };
    }

    private void Start()
    {
        StartCoroutine(Initialize());
    }

    private IEnumerator Initialize ()
    {
        try
        {
            serialPort.Open();
            portOpen = true;
        }
        catch (Exception err)
        {
            Debug.Log(string.Format("Deu pau na porta: {0}",err.Message));
            portOpen = false;
        }

        yield return new WaitForSeconds(1f);
        isReady = true;
	}

    private void OnDestroy()
    {
        serialPort.Close();
        portOpen = false;
    }
		
	private void Update ()
    {
        if (portOpen)
        {
            string read = serialPort.ReadLine();
            if (DebugMode && DebugText != null)
            {
                DebugText.text = read;
            }

            currentValue = int.Parse(read);
             

            //TODO: EXPOSE THIS VARIABLES TO MAKE IT EASIER TO RE-SKIN THE PROJECT
            if(currentValue > 0)
            {
                currentValue = 0;
            }

            if(currentValue < -80000)
            {
                currentValue = -80000;
            }

            if(currentValue > lastValue)
            {
                if(lastValue != 0)
                {
                    float growth =  Math.Abs(currentValue)/(float)Math.Abs(lastValue);
                    velocity = growth;
                }
                isMovingRight = true;
                isMovingLeft = false;
                isStoped = false;
                lastValue = currentValue;
            }
            else if(currentValue < lastValue)
            {
                if(lastValue != 0)
                {
                    float growth =  Math.Abs(lastValue)/(float)Math.Abs(currentValue);
                    velocity = growth;
                }
                
                isMovingLeft = true;
                isMovingRight = false;
                isStoped = false;
                lastValue = currentValue;
            }
            else if(currentValue == lastValue)
            {
                isStoped = true;
                isMovingLeft = false;
                isMovingRight = false;
                lastValue = currentValue;
            }
        }
	}
    
    public float getVelocity()
    {
        return velocity;
    }

    public void SwitchPort(Dropdown dropdown)
    {
        string port = dropdown.options[dropdown.value].text;

        serialPort.Close();
        portOpen = false;

        PlayerPrefs.SetString("PortName", port);   
        portName = port;

        serialPort = new SerialPort(portName)
        {
            DtrEnable = true
        };

        StartCoroutine(Initialize());
    }
}
