﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentSpawner : MonoBehaviour 
{
	public Transform contentPanel;

	public GameObject[] contentList;
	public Vector3[] contentPositions;
	
}
