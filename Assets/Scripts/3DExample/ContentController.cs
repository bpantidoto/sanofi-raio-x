﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentController : MonoBehaviour 
{
	public float constrainLeft;
	public float constrainRight;
	public Transform cameraTransform;

	private EncoderReader _encoder;
	private void Awake() 
	{
		_encoder = FindObjectOfType<EncoderReader>();
	}

	private void Update()
	{
		if(_encoder != null)
		{
			if(_encoder.isReady)
			{
				if(_encoder.isMovingRight)
				{
					MoveRight();
				}else if (_encoder.isMovingLeft)
				{
					MoveLeft();
				}
			}
		}
	}

	private void MoveLeft()
	{
		float newPosition = cameraTransform.position.x-(GlobalParameters.GetMultiplier() * _encoder.getVelocity());
		if(newPosition < constrainLeft)
		{
			newPosition = constrainLeft;
		}

		cameraTransform.position = new Vector3(newPosition, 0, 0);
	}	

	private void MoveRight()
	{
		float newPosition = cameraTransform.position.x+(GlobalParameters.GetMultiplier() * _encoder.getVelocity());
		if(newPosition > constrainRight)
		{
			newPosition = constrainRight;
		}

		cameraTransform.position = new Vector3(newPosition, 0, 0);
	}
}
