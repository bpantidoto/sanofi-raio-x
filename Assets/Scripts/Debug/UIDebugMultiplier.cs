﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIDebugMultiplier : MonoBehaviour 
{
	public Slider sliderValue;
	public Text textValue;

	private void Awake()
	{
		sliderValue.value = GlobalParameters.GetMultiplier();
	}

	public void onValueChange()
	{
		textValue.text = sliderValue.value.ToString();
		GlobalParameters.SetMultiplier(sliderValue.value);
	}
}
