﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsManager : MonoBehaviour
{
	private int _points = 0;
	public int points {
		get {
			return _points;
		}
	}

	public int[] pointsByExercise;

	public void AddPoints(int quantity)
	{
		_points += quantity;
	}

	public void SubtractPoints(int quantity)
	{
		_points -= quantity;
		if(_points < 0)
		{
			_points = 0;
		}
	}
}