﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalParameters : MonoBehaviour 
{
	private static float _multiplier = 0.01f;

	private void Awake()
	{
		Scene currentScene = SceneManager.GetActiveScene();
		switch(currentScene.name)
		{
			case("Scroll_Grid"):
				_multiplier = PlayerPrefs.GetFloat("ScrollMultiplier");
			break;
			case("Scroll_No_Grid"):
				_multiplier = PlayerPrefs.GetFloat("DefaultMultiplier");
			break;
		}
	}

	public static void SetMultiplier(float value)
	{
		_multiplier = value;

		Scene currentScene = SceneManager.GetActiveScene();
		switch(currentScene.name)
		{
			case("Scroll_Grid"):
				PlayerPrefs.SetFloat("ScrollMultiplier", _multiplier);
			break;
			case("Scroll_No_Grid"):
				PlayerPrefs.SetFloat("DefaultMultiplier", _multiplier);
			break;
		}
	}

	public static float GetMultiplier()
	{
		Scene currentScene = SceneManager.GetActiveScene();
		switch(currentScene.name)
		{
			case("Scroll_Grid"):
				_multiplier = PlayerPrefs.GetFloat("ScrollMultiplier");
			break;
			case("Scroll_No_Grid"):
				_multiplier = PlayerPrefs.GetFloat("DefaultMultiplier");
			break;
		}

		return _multiplier;
	}
}
