﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundRelativeMovement : MonoBehaviour 
{
	private EncoderReader _encoder;

	public Transform targetTransform;
	public float totalWidth = 77557f;

	public float initialPosition = 1647;
	public float finalPosition = -1042;

	private void Awake()
	{
		_encoder = FindObjectOfType<EncoderReader>();
	}

	private void Update()
	{
		//TODO: RELATIVE MOVEMENT
		float percent = _encoder.currentValue/totalWidth;
		float difference = FindDifference();
		float onePercentOfDifference = difference/100;
		float step = onePercentOfDifference*(percent*100);
		//targetRect.localPosition = new Vector3(initialPosition + step, targetRect.localPosition.y, 0);

		//Debug.Log(initialPosition);
		//step = 0;
		targetTransform.localPosition = new Vector3(initialPosition + step, targetTransform.localPosition.y, 0);
	}

	public float FindDifference()
	{
		return Mathf.Abs(Mathf.Abs(finalPosition) + Mathf.Abs(initialPosition));
	}
}

