﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamManager : MonoBehaviour
{
	public Color[] availableTeamColors;

	private int _currentTeam = 0;
	private string _teamName = "";
	public string teamName {
		get {
			return _teamName;
		}
	}

	private Color _teamColor;
	public Color teamColor {
		get {
			return _teamColor;
		}
	}

	public CustomEvent OnSetTeam;

	public void SetTeamID(int id)
	{
		_currentTeam = id;
		_teamColor = availableTeamColors[_currentTeam];

		if(OnSetTeam != null)
		{
			OnSetTeam.Invoke();
		}
	}

	public void SetTeamName(string teamName)
	{
		_teamName = teamName;
		Debug.Log(_teamName);
	}

	public void CreateDatabaseEntry()
	{
		DatabaseManager databaseManager = GameObject.FindObjectOfType<DatabaseManager>();
		databaseManager.CreateNewUser(_teamName);
	}
		
	public void ResetTeam()
	{
		
	}
}
