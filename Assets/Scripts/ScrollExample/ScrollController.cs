﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollController : Singleton<ScrollController> 
{
	public ScrollRect scrollRect;

	private EncoderReader _encoder;
	private void Awake() 
	{
		_encoder = FindObjectOfType<EncoderReader>();
	}

	private void Update()
	{
		if(_encoder != null)
		{
			if(_encoder.isReady)
			{
				if(_encoder.isMovingRight)
				{
					scrollRect.horizontalNormalizedPosition  += (GlobalParameters.GetMultiplier() * _encoder.getVelocity());
				}else if (_encoder.isMovingLeft)
				{
					scrollRect.horizontalNormalizedPosition  -= (GlobalParameters.GetMultiplier() * _encoder.getVelocity());
				}
			}
		}
	}	
}
