﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncoderContentMotion : MonoBehaviour 
{
	public RectTransform rectTransform;

	private Vector3 _startPosition;
	private EncoderReader _encoder;

	private void Awake() 
	{
		_startPosition = rectTransform.localPosition;
		_encoder = FindObjectOfType<EncoderReader>();
	}

	private void Update()
	{
		if(_encoder != null)
		{
			if(_encoder.isReady)
			{
				if(_encoder.isMovingLeft)
				{
					float value = (GlobalParameters.GetMultiplier() * _encoder.getVelocity());
					Vector3 newPosition = new Vector3(rectTransform.localPosition.x + value, rectTransform.localPosition.y, rectTransform.localPosition.z);
					rectTransform.localPosition = newPosition;
				}else if (_encoder.isMovingRight)
				{
					float value = (GlobalParameters.GetMultiplier() * _encoder.getVelocity());
					Vector3 newPosition = new Vector3(rectTransform.localPosition.x - value, rectTransform.localPosition.y, rectTransform.localPosition.z);
					rectTransform.localPosition = newPosition;
				}

				//CONSTRAINS SO CONTENT WONT GET OFFSCREEN
				if(rectTransform.localPosition.x > _startPosition.x)
				{
					rectTransform.localPosition = _startPosition;
				}

				//Think of a way to get rid of the hardcoded value
				if(rectTransform.localPosition.x < -8100)
				{
					Vector3 newPosition = new Vector3(-8100, rectTransform.localPosition.y, rectTransform.localPosition.z);
					rectTransform.localPosition = newPosition;
				}
			}
		}
	}	
}