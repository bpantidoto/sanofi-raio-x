﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentMotionController : MonoBehaviour 
{
	public RectTransform rectTransform;
	private EncoderReader _encoder;

	public int initialValue = 0;
	public int maxValue = 84000;

	private Vector3 _initialPosition;

	private void Awake() 
	{
		_encoder = FindObjectOfType<EncoderReader>();
		_initialPosition = rectTransform.localPosition;
	}

	private void Update()
	{
		if(_encoder != null)
			rectTransform.localPosition = new Vector3(_initialPosition.x + _encoder.currentValue, rectTransform.localPosition.y, rectTransform.localPosition.z);
	}
	
}
