﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExercicePoints : MonoBehaviour 
{
	public int exerciseValue = 100;
	public int wrongAnswerCost = 10;
	public int exerciseIndex;

	public UIDragNDropVerifier[] verifiers;

	public UIOpenQuestionButton[] buttons;
	public UIButtonOpenDoubleQuestion[] buttonsDouble;

	public UIButtonOpenQuestionTriple[] buttonsTripleQuestion;

	private PointsManager _pointsManager;

	private void Start()
	{
		_pointsManager = GameObject.FindGameObjectWithTag("PointsManager").GetComponent<PointsManager>();
	}
	
	public int CalculatePoints()
	{
		return _pointsManager.pointsByExercise[exerciseIndex];
	}

	public void AddRightAnswer()
	{
		_pointsManager.pointsByExercise[exerciseIndex] += exerciseValue;
	}

	public void AddWrongAnswer()
	{
		_pointsManager.pointsByExercise[exerciseIndex] -= wrongAnswerCost;
		if(_pointsManager.pointsByExercise[exerciseIndex] < 0)
		{
			_pointsManager.pointsByExercise[exerciseIndex] = 0;
		}
	}

	public int GetTries()
	{
		int totalTries = 0;
		if(verifiers.Length > 0)
		{
			foreach(UIDragNDropVerifier current in verifiers)
			{
				totalTries += current.totalTries;
			}
		}

		if(buttons.Length > 0)
		{
			foreach(UIOpenQuestionButton current in buttons)
			{
				totalTries += current.tries;
			}
		}

		if(buttonsDouble.Length > 0)
		{
			foreach(UIButtonOpenDoubleQuestion current in buttonsDouble)
			{
				totalTries += current.tries;
			}
		}


		if(buttonsTripleQuestion.Length>0)
		{
			foreach(UIButtonOpenQuestionTriple current in buttonsTripleQuestion)
			{
				if(!current.asnweredCorrectly)
				{
					totalTries++;
				}
			}
		}
			
		return totalTries;
	}
}
