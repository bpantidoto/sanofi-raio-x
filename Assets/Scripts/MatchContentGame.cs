﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

public class MatchContentGame : MonoBehaviour 
{
	private UIMatchContent[] _contents;
	private CanvasGroup _canvasGroup;

	private void Awake() 
	{
		_contents = GetComponentsInChildren<UIMatchContent>();
		_canvasGroup = GetComponent<CanvasGroup>();
	}

	public void FinishGame()
	{
		//TODO: SAVE CORRECT ANSWERS
		//TODO: ADD PONTUATION SYSTEM
		_canvasGroup.DOFade(0f, 1f);
	}
}