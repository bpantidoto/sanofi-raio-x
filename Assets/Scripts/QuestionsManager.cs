﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionsManager : MonoBehaviour 
{
	public Question[] questions;
}

[System.Serializable]
public class Question 
{
	public string title;
	public string[] answers;
	public int correctAnswer;
}
